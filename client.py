#!/usr/bin/env python3

from chatlib import prompt
from chatlib import net

from ipaddress import IPv4Address, IPv4Network, AddressValueError
from os import listdir

def get_network():
    ifaces = listdir('/sys/class/net')
    ips = [net.get_ip_from_iface(i) for i in ifaces]
    options = zip(ifaces, ips)
    sel = prompt.select('Select an interface to use:', [f'{i[0]}: {i[1]}' for i in options])
    if 0 <= sel < len(ifaces):
        mask = net.get_subnet_from_iface(ifaces[sel])
        return IPv4Network(f'{ips[sel]}/{mask}', False)
    return None

def get_server(network, port):
    server = None
    options = [
        'Enter Server IPv4 Address',
        f'Search In {network} For Server'
    ]
    sel = prompt.select('Locate Chat Server:', options)
    if sel == 0:
        try:
            server = IPv4Address(input('[+] Enter address: '))
        except AddressValueError:
            print('[-] Invalid IPv4 Address')
    elif sel == 1:
        server = net.find_server(network, port)
        if server is None:
            print('[-] Could not locate server on network.')
    return server

def main():
    CHAT_PORT = 31337
    network = get_network()
    if network is None:
        return 1
    
    server = get_server(network, CHAT_PORT)
    if server is None:
        return 2
    
    print(f'[+] Found server at {server}.')

if __name__ == '__main__':
    main()