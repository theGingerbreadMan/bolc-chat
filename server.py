#!/usr/bin/env python3

from typing import Tuple
from chatlib import prompt
from chatlib import net

from os import listdir

def get_iface():
    ifaces = listdir('/sys/class/net')
    ips = [net.get_ip_from_iface(i) for i in ifaces]
    options = zip(ifaces, ips)
    sel = prompt.select('Select an interface to use:', [f'{i[0]}: {i[1]}' for i in options])
    if 0 <= sel < len(ifaces):
        print('[+] Using', ifaces[sel])
        return ips[sel]
    return False

def main():
    iface = get_iface()
    if iface == False:
        return -1
    server = net.start_server(iface, 31337)
    net.run_server(server)

if __name__ == '__main__':
    main()
