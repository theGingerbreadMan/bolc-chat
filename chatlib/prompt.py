def select(prompt:str, options:list):
    choice = -1
    while choice < 0:
        print('[+]', prompt)
        for idx in range(len(options)):
            print(f'\t[{idx}] {options[idx]}')
        print(f'\t[{idx + 1}] Exit')
        msg = input('> ')
        if msg.isdigit() and int(msg) <= len(options):
            choice = int(msg)
        else:
            print('[!] Invalid Selection')
    return choice

def prompt():
    user_in = input('> ')
    if user_in == '/exit':
        return False
    return user_in
