from socket import socket, inet_ntoa, timeout, AF_INET, SOCK_DGRAM, SOCK_STREAM
from signal import signal, SIGINT
from struct import pack
from fcntl import ioctl

def get_ip_from_iface(name):
    s = socket(AF_INET, SOCK_DGRAM)
    return inet_ntoa(ioctl(
        s.fileno(),
        0x8915, # SIOCGIFADDR
        pack('256s', name.encode()[:15])
    )[20:24])

def get_subnet_from_iface(name):
    s = socket(AF_INET, SOCK_DGRAM)
    return inet_ntoa(ioctl(
        s.fileno(),
        0x891B,
        pack('256s', name.encode()[:15])
    )[20:24])

### CLIENT FUNCTIONS ###
def find_server(network, port):
    server = None
    print('[+] Searching for server...')
    for host in network.hosts():
        s = socket(AF_INET, SOCK_STREAM)
        s.settimeout(0.1)
        open = (s.connect_ex((str(host), port)) == 0)
        s.close()
        if open:
            server = host
            break
    return server

### SERVER FUNCTIONS ###
def start_server(ip, port):
    print(f'[+] Starting server on {ip}:{port}')
    sock = socket(AF_INET, SOCK_STREAM)
    sock.bind((ip, port))
    sock.settimeout(1)
    sock.listen(1)
    return sock

shutdown = False
def signal_shutdown(signum, frame):
    print('[+] Received shutdown signal.')
    global shutdown
    shutdown = True

def run_server(sock:socket):
    global shutdown
    signal(SIGINT, signal_shutdown)
    while not shutdown:
        try:
            conn, addr = sock.accept()
        except timeout:
            continue
        with conn:
            print('Connection from', addr)
        conn.close()
    
    print('[+] Shutting down.')
    sock.close()
